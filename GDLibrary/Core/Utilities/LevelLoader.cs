﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Containers;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary.Utilities
{
    /// <summary>
    /// Use the level loader to instanciate 3D drawn actors within your level from a PNG file.
    ///
    /// Usage:
    ///    LevelLoader levelLoader = new LevelLoader(this.objectArchetypeDictionary,
    ///    this.textureDictionary);
    ///     List<DrawnActor3D> actorList = levelLoader.Load(this.textureDictionary[fileName],
    ///           scaleX, scaleZ, height, offset);
    ///     this.object3DManager.Add(actorList);
    ///
    /// </summary>
    public class LevelLoader<T> where T : DrawnActor3D
    {
        private static readonly Color ColorLevelLoaderIgnore = Color.White;

        private Dictionary<string, T> archetypeDictionary;
        private ContentDictionary<Texture2D> textureDictionary;

        public LevelLoader(Dictionary<string, T> archetypeDictionary,
            ContentDictionary<Texture2D> textureDictionary)
        {
            this.archetypeDictionary = archetypeDictionary;
            this.textureDictionary = textureDictionary;
        }

        public List<DrawnActor3D> Load(Texture2D texture,
            float scaleX, float scaleZ, float height, Vector3 offset)
        {
            List<DrawnActor3D> list = new List<DrawnActor3D>();
            Color[] colorData = new Color[texture.Height * texture.Width];
            texture.GetData<Color>(colorData);

            Color color;
            Vector3 translation;
            DrawnActor3D actor;

            for (int y = 0; y < texture.Height; y++)
            {
                for (int x = 0; x < texture.Width; x++)
                {
                    color = colorData[x + y * texture.Width];

                    if (!color.Equals(ColorLevelLoaderIgnore))
                    {
                        //scale allows us to increase the separation between objects in the XZ plane
                        translation = new Vector3(x * scaleX, height, y * scaleZ);

                        //the offset allows us to shift the whole set of objects in X, Y, and Z
                        translation += offset;

                        actor = getObjectFromColor(color, translation);

                        if (actor != null)
                        {
                            list.Add(actor);
                        }
                    }
                } //end for x
            } //end for y
            return list;
        }

        private Random rand = new Random();

        private int count = 1;
        private int countMoveable = 1;

        private DrawnActor3D getObjectFromColor(Color color, Vector3 translation)
        {
            if (color.Equals(new Color(0, 255, 0)))
            {
                CollidablePrimitiveObject collidablePrimitiveObject = archetypeDictionary[GameConstants.Primitive_LitTexturedSwitch1] as CollidablePrimitiveObject;
                collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                //change it a bit
                collidablePrimitiveObject.Transform3D = transform3D;
                collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;

                return collidablePrimitiveObject;
            }
            else if (color.Equals(new Color(0, 200, 0)))
            {
                CollidablePrimitiveObject collidablePrimitiveObject = archetypeDictionary[GameConstants.Primitive_LitTexturedSwitch2] as CollidablePrimitiveObject;
                collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                //change it a bit
                collidablePrimitiveObject.Transform3D = transform3D;
                collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;

                return collidablePrimitiveObject;
            }
            else if (color.Equals(new Color(0, 150, 0)))
            {
                CollidablePrimitiveObject collidablePrimitiveObject = archetypeDictionary[GameConstants.Primitive_LitTexturedSwitch3] as CollidablePrimitiveObject;
                collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                //change it a bit
                collidablePrimitiveObject.Transform3D = transform3D;
                collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;

                return collidablePrimitiveObject;
            }
            else if (color.Equals(new Color(0, 0, 255)))
            {
                // WATER TILES

                CollidablePrimitiveObject collidablePrimitiveObject = archetypeDictionary[GameConstants.Primitive_LitTexturedWater] as CollidablePrimitiveObject;
                collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

                Transform3D transform3D = new Transform3D(translation - new Vector3(0, 4f, 0), new Vector3(0, 0, 0), new Vector3(10, 1, 10), Vector3.UnitZ, Vector3.UnitY);
                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                //change it a bit
                collidablePrimitiveObject.ID = "water" + count++;
                collidablePrimitiveObject.Transform3D = transform3D;
                collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;

                return collidablePrimitiveObject;

            }
            else if (color.Equals(new Color(255, 0, 0)))
            {
                CollidablePrimitiveObject collidablePrimitiveObject = archetypeDictionary[GameConstants.Primitive_LitTexturedCube] as CollidablePrimitiveObject;
                collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                //change it a bit
                collidablePrimitiveObject.ID = "block" + count++;
                collidablePrimitiveObject.Transform3D = transform3D;
                collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;
                collidablePrimitiveObject.EffectParameters.Texture = textureDictionary["redWallBricks"];

                return collidablePrimitiveObject;
            }
            else if (color.Equals(new Color(200, 0, 0)))
            {
                CollidablePrimitiveObject moveableBrick = archetypeDictionary[GameConstants.Primitive_LitTexturedMoveableBlock1] as CollidablePrimitiveObject;
                moveableBrick = moveableBrick.Clone() as CollidablePrimitiveObject;
                
                translation.X += 5f;
                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * new Vector3(2, 1, 1), Vector3.UnitZ, Vector3.UnitY);

                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                moveableBrick.Transform3D = transform3D;
                moveableBrick.CollisionPrimitive = collisionPrimitive;

                return moveableBrick;
            }
            else if (color.Equals(new Color(150, 0, 0)))
            {
                CollidablePrimitiveObject moveableBrick = archetypeDictionary[GameConstants.Primitive_LitTexturedMoveableBlock2] as CollidablePrimitiveObject;
                moveableBrick = moveableBrick.Clone() as CollidablePrimitiveObject;

                translation.Z += 5f;
                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * new Vector3(1, 1, 2), Vector3.UnitZ, Vector3.UnitY);

                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                moveableBrick.Transform3D = transform3D;
                moveableBrick.CollisionPrimitive = collisionPrimitive;

                return moveableBrick;
            }
            else if (color.Equals(new Color(100, 0, 0)))
            {
                CollidablePrimitiveObject moveableBrick = archetypeDictionary[GameConstants.Primitive_LitTexturedMoveableBlock3] as CollidablePrimitiveObject;
                moveableBrick = moveableBrick.Clone() as CollidablePrimitiveObject;

                translation.Z += 5f;
                Transform3D transform3D = new Transform3D(translation, Vector3.Zero, 10 * new Vector3(1, 1, 2), Vector3.UnitZ, Vector3.UnitY);

                BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform3D);

                moveableBrick.Transform3D = transform3D;
                moveableBrick.CollisionPrimitive = collisionPrimitive;

                return moveableBrick;
            }
            else if (color.Equals(new Color(0, 0, 250)))
            {
                //enemy instance
                return null;
            }
            //add an else if for each type of object that you want to load...

            return null;
        }
    }
}