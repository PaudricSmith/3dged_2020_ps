﻿using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary.Controllers
{
    public class FixedController : TargetController
    {
        private float elevationAngleInDegrees;
        private float distanceFromTarget;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private float fixedMoveSpeed;
        private float fixedStrafeSpeed;
        private Vector3 oldCameraTranslation;

        public FixedController(string id, ControllerType controllerType,
            IActor targetActor, float elevationAngleInDegrees, float distanceFromTarget,
            KeyboardManager keyboardManager, MouseManager mouseManager,
            float fixedMoveSpeed, float fixedStrafeSpeed) : base(id, controllerType, targetActor)
        {
            this.elevationAngleInDegrees = elevationAngleInDegrees;
            this.distanceFromTarget = distanceFromTarget;
            this.keyboardManager = keyboardManager;
            this.mouseManager = mouseManager;
            this.fixedMoveSpeed = fixedMoveSpeed;
            this.fixedStrafeSpeed = fixedStrafeSpeed;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor3D parent = actor as Actor3D;

            if (parent != null)
            {
                HandleKeyboardInput(gameTime, parent);
            }

            base.Update(gameTime, actor);
        }

        private void HandleKeyboardInput(GameTime gameTime, Actor3D parent)
        {
            Actor3D target = this.TargetActor as Actor3D; //car

            Vector3 moveVector = Vector3.Zero;

            //if (keyboardManager.IsKeyDown(Keys.W))
            //{
            //    moveVector = target.Transform3D.Look;
            //}
            //else if (keyboardManager.IsKeyDown(Keys.S))
            //{
            //    moveVector = -1 * target.Transform3D.Look;
            //}
            //else if (keyboardManager.IsKeyDown(Keys.A))
            //{
            //    moveVector = -1 * target.Transform3D.Right;
            //}
            //else if (keyboardManager.IsKeyDown(Keys.D))
            //{
            //    moveVector = target.Transform3D.Right;
            //}

            //constrain movement in Y-axis to stop object moving up/down in space
            //moveVector.Y = 0;
            float x = target.Transform3D.Translation.X;
            float y = target.Transform3D.Translation.Y;
            float z = target.Transform3D.Translation.Z;
            moveVector.X = x;
            moveVector.Y = y;
            moveVector.Z = z;


            //parent.Transform3D.Look = target.Transform3D.Look;
            //moveVector = parent.Transform3D.Look;

            //moveVector.X = target.Transform3D.Translation.X;
            //moveVector.Z = target.Transform3D.Translation.Z;
            
            //final camera position <- intermediate camera positions
            //moveVector = target.Transform3D.Translation;
            //parent.Transform3D.TranslateBy(moveVector * gameTime.ElapsedGameTime.Milliseconds);

            //store the old camera for the next round of lerp in next update(
            //this.oldCameraTranslation = parent.Transform3D.Translation;

            //apply the forward/backward movement
           // parent.Transform3D.TranslateBy(moveVector);
        }

        //to do...add Clone, Equals, GetHashCode
    }
}