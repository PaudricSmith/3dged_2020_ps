﻿using GDGame;
using GDGame.MyGame.Managers;
using GDLibrary.Actors;
using GDLibrary.Containers;
using GDLibrary.Controllers;
using GDLibrary.Debug;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.GameComponents;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary.Core.Managers.State
{
    /// <summary>
    /// Use this manager to listen for related events and perform actions in your game based on events received
    /// </summary>
    public class MyGameStateManager : PausableGameComponent, IEventHandler
    {
        //private UITextObject timeTextObject;
        //private float timerCount = 0;

        private ObjectManager objectManager;
        private MyMenuManager myMenuManager;
        private CameraManager<Camera3D> cameraManager;

        private static bool startTimer = false;
        private static bool level1End = false;
        private static bool level1Reset = false;
        private static bool timerReset = false;
        private static bool firstPlay = true;
        private static bool inGame = false;
        private static bool playButtonPressed = false;

        private bool tetraPlayer = true;
        private bool hexaPlayer = false;
        private bool octaPlayer = false;

        private bool menuThemePlaying = true;
        private bool tetraThemePlaying = true;
        private bool hexaThemePlaying = true;
        private bool octaThemePlaying = true;


        public bool StartTimer { get => startTimer; set => startTimer = value; }
        public bool Level1End { get => level1End; set => level1End = value; }
        public bool TimerReset { get => timerReset; set => timerReset = value; }
        public bool Level1Reset { get => level1Reset; set => level1Reset = value; }
        public bool InGame { get => inGame; set => inGame = value; }
        public bool PlayButtonPressed { get => playButtonPressed; set => playButtonPressed = value; }

        public MyGameStateManager(Game game, StatusType statusType, ObjectManager objectManager, MyMenuManager myMenuManager, CameraManager<Camera3D> cameraManager) : base(game, statusType)
        {
            this.objectManager = objectManager;
            this.myMenuManager = myMenuManager;
            this.cameraManager = cameraManager;
        }

        public override void SubscribeToEvents()
        {
            EventDispatcher.Subscribe(EventCategoryType.Menu, HandleEvent);
            EventDispatcher.Subscribe(EventCategoryType.Level, HandleLevelEvent);
            EventDispatcher.Subscribe(EventCategoryType.Player, HandlePlayerEvent);

            base.SubscribeToEvents();
        }

        public override void HandleEvent(EventData eventData)
        {
            if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnPlay)
            {
                object[] parameters = { "menuTheme" };
                object[] parameters2 = { "tetTheme" };
                //object[] parameters3 = { "hexTheme" };
                object[] parameters3 = { "hexTheme2" };
                object[] parameters4 = { "octTheme" };

                if (menuThemePlaying)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));
                    menuThemePlaying = false;
                }

                else if (tetraPlayer && tetraThemePlaying)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
                    tetraThemePlaying = false;
                }
                else if (hexaPlayer && hexaThemePlaying)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters3));
                    hexaThemePlaying = false;
                }
                else if (octaPlayer && octaThemePlaying)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters4));
                    octaThemePlaying = false;
                }

                if (InGame == true)
                {
                    StartTimer = true;
                }

                //startTimer = true;

                //firstPlay = false;

                PlayButtonPressed = true;
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnPause)
            {
                object[] parameters = { "menuTheme" };
                object[] parameters2 = { "tetTheme" };
                //object[] parameters3 = { "hexTheme" };
                object[] parameters3 = { "hexTheme2" };
                object[] parameters4 = { "octTheme" };
                object[] parameters5 = { "tetToHex" };
                object[] parameters6 = { "hexToOcta" };

                if (menuThemePlaying == false)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters2));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters3));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters4));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters5));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters6));

                    menuThemePlaying = true;
                }

                if (tetraPlayer && tetraThemePlaying == false)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters2));
                    tetraThemePlaying = true;
                }
                if (hexaPlayer && hexaThemePlaying == false)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters3));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters5));
                    hexaThemePlaying = true;
                }
                if (octaPlayer && octaThemePlaying == false)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters4));
                    octaThemePlaying = true;
                }


                this.myMenuManager.SetScene("retry");
                StartTimer = false;

                PlayButtonPressed = false;

            }

            //remember to pass the eventData down so the parent class can process pause/unpause
            base.HandleEvent(eventData);
        }

        private void HandleLevelEvent(EventData eventData)
        {
            if (eventData.EventActionType == EventActionType.OnL1End)
            {
                this.InGame = false;
                this.Level1End = true;
                this.StartTimer = false;
                this.TimerReset = true;
                this.Level1Reset = true;

                this.objectManager.Clear();

            }
            else if (eventData.EventActionType == EventActionType.OnL1Retry)
            {
                this.Level1End = true;
                this.TimerReset = true;
                this.Level1Reset = true;

                this.objectManager.Clear();

            }
            else if (eventData.EventActionType == EventActionType.OnStartTimer)
            {
                this.StartTimer = true;
            }
        }

        private void HandlePlayerEvent(EventData eventData)
        {
            if (eventData.EventActionType == EventActionType.OnTetraStart)
            {
                object[] parameters = { "tetArrive" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                tetraPlayer = true;
                this.InGame = true;
            }
            else if (eventData.EventActionType == EventActionType.OnHexaStart)
            {
                object[] parameters = { "tetTheme" };
                object[] parameters2 = { "tetToHex" };
                //object[] parameters3 = { "hexTheme" };
                object[] parameters3 = { "hexTheme2" };

                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters3));

                hexaPlayer = true;
                tetraPlayer = false;
            }
            else if (eventData.EventActionType == EventActionType.OnOctaStart)
            {
                //object[] parameters = { "hexTheme" };
                object[] parameters = { "hexTheme2" };
                object[] parameters2 = { "hexToOcta" };
                object[] parameters3 = { "octTheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters3));

                octaPlayer = true;
                hexaPlayer = false;
            }
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            //add code here to check for the status of a particular set of related events e.g. collect all inventory items then...
            
            //if (startTimer)
            //{
            //    timerCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
            //    timeTextObject.Text = timerCount.ToString("0.00");
            //}

            base.ApplyUpdate(gameTime);
        }
    }
}