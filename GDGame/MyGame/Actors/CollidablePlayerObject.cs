﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Controllers;
using GDLibrary.Core.Controllers;
using GDLibrary.Core.Managers.State;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    /// <summary>
    /// Moveable, collidable player using keyboard and checks for collisions
    /// </summary>
    public class CollidablePlayerObject : CollidablePrimitiveObject
    {
        #region Fields
        private float moveSpeed, strafeSpeed;
        private KeyboardManager keyboardManager;
        private MyGameStateManager myGameStateManager;
        private Keys[] moveKeys;

        private Game game;

        private bool goalThemePlaying = true;
        private bool switch1EffectPlaying = true;
        private bool switch2EffectPlaying = true;
        private bool switch3EffectPlaying = true;
        private static bool switch1 = false;
        private static bool switch2 = false;
        private static bool switch3 = false;

        private float tickCount = 0;
        private float endZoneTickCount = 0;
        #endregion Fields

        public CollidablePlayerObject(string id, ActorType actorType, StatusType statusType, Transform3D transform,
            EffectParameters effectParameters, IVertexData vertexData,
            ICollisionPrimitive collisionPrimitive, ObjectManager objectManager,
            Game game,
            MyGameStateManager myGameStateManager,
            Keys[] moveKeys, float moveSpeed, float strafeSpeed, 
            KeyboardManager keyboardManager)
            : base(id, actorType, statusType, transform, effectParameters, vertexData, collisionPrimitive, objectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.strafeSpeed = strafeSpeed;

            //for movement
            this.keyboardManager = keyboardManager;
            this.myGameStateManager = myGameStateManager;

            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {
            //read any input and store suggested increments
            HandleInput(gameTime);

            //have we collided with something?
            Collidee = CheckAllCollisions(gameTime);

            //how do we respond to this collidee e.g. pickup?
            HandleCollisionResponse(Collidee, gameTime);

            //if no collision then move - see how we set this.Collidee to null in HandleCollisionResponse()
            //below when we hit against a zone
            if (Collidee == null)
            {
                ApplyInput(gameTime);
            }

            //reset translate and rotate and update primitive
            base.Update(gameTime);
        }

        protected override void HandleInput(GameTime gameTime)
        {
            if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])) //Forward
            {
                Transform3D.TranslateIncrement = Transform3D.Look * gameTime.ElapsedGameTime.Milliseconds * moveSpeed;
            }
            else if (keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])) //Backward
            {
                Transform3D.TranslateIncrement = -Transform3D.Look * gameTime.ElapsedGameTime.Milliseconds * moveSpeed;
            }
            else if (keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])) //Left
            {
                Transform3D.TranslateIncrement = -Transform3D.Right * gameTime.ElapsedGameTime.Milliseconds * strafeSpeed;
            }
            else if (keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7])) //Right
            {
                Transform3D.TranslateIncrement = Transform3D.Right * gameTime.ElapsedGameTime.Milliseconds * strafeSpeed;
            }
        }

        /********************************************************************************************/

        //this is where you write the application specific CDCR response for your game
        protected override void HandleCollisionResponse(Actor collidee, GameTime gameTime)
        {
            if (collidee is CollidableZoneObject)
            {
                CollidableZoneObject simpleZoneObject = collidee as CollidableZoneObject;

                //do something based on the zone type - see Main::InitializeCollidableZones() for ID
                if (simpleZoneObject.ID.Equals("endZone"))
                {
                    object[] parameters = { "menuTheme" };
                    object[] parameters2 = { "tetTheme" };
                    object[] parameters3 = { "octTheme" };

                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters2));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters3));

                    if (goalThemePlaying)
                    {
                        object[] additionalParameters = { "endZoneTheme" };
                        EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, additionalParameters));

                        goalThemePlaying = false;
                    }

                    if (this.Transform3D.Translation.Z > 135)
                    {
                        this.Transform3D.Translation = new Vector3(100, 21, 140);

                        this.ControllerList.Add(
                        new RotationController("rot controller1", ControllerType.RotationOverTime,
                        1, new Vector3(0, 0.5f, 0)));
                    }
                    
                    endZoneTickCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (endZoneTickCount >= 3)
                    {
                        
                        EventDispatcher.Publish(new EventData(EventCategoryType.Level, EventActionType.OnL1End, null));

                        //EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));

                        ObjectManager.Remove(this);
                        
                    }

                    myGameStateManager.StartTimer = false;
                }

                //IMPORTANT - setting this to null means that the ApplyInput() method will get called and the player can move through the zone.
                Collidee = null;
            }
            else if (collidee is CollidablePrimitiveObject)
            {
                //the boxes on the left that we loaded from level loader
                if (collidee.ActorType == ActorType.CollidablePickup)
                {
                    //remove the object
                    object[] parameters = { collidee };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Object, EventActionType.OnRemoveActor, parameters));
                }
                //the boxes on the right that move up and down
                else if (collidee.ActorType == ActorType.CollidableDecorator)
                {
                    //(collidee as DrawnActor3D).EffectParameters.DiffuseColor = Color.Blue;
                    //CollidableSwitch
                }
                else if (this.ID == "collidableTetra" && collidee.ActorType == ActorType.Switch1) // ONLY TETRAHEDRON CAN ACTIVATE SWITCHES
                {
                    switch1 = true;
                    (collidee as DrawnActor3D).EffectParameters.DiffuseColor = new Color(139, 63, 150);

                    if (switch1EffectPlaying)
                    {
                        // 'mechanical menu-button press.wav' thanks to 'Lavacoal123' at 'freesound.org'
                        // https://freesound.org/people/Lavacoal123/sounds/427870/ 

                        object[] parameters = { "switch" };
                        EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                        switch1EffectPlaying = false;
                    }

                }
                else if (this.ID == "collidableTetra" && collidee.ActorType == ActorType.Switch2) // ONLY TETRAHEDRON CAN ACTIVATE SWITCHES
                {
                    switch2 = true;
                    (collidee as DrawnActor3D).EffectParameters.DiffuseColor = new Color(139, 63, 150);

                    if (switch2EffectPlaying)
                    {
                        object[] parameters = { "switch" };
                        EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                        switch2EffectPlaying = false;
                    }
                }
                else if (this.ID == "collidableTetra" && collidee.ActorType == ActorType.Switch3) // ONLY TETRAHEDRON CAN ACTIVATE SWITCHES
                {
                    switch3 = true;
                    (collidee as DrawnActor3D).EffectParameters.DiffuseColor = new Color(139, 63, 150);

                    if (switch3EffectPlaying)
                    {
                        object[] parameters = { "switch" };
                        EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                        switch3EffectPlaying = false;
                    }
                }
                else if (this.ID == "collidableHexa" && (collidee.ActorType == ActorType.MoveableBlock1 && switch1)) // ONLY HEXAHEDRON CAN MOVE BLOCKS WHEN THEY HAVE BEEN SWITCHED ON
                {
                    if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])) //Forward
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0, 0, -0.01f) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])) //Backward
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0, 0, 0.01f) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])) //Left
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(-0.01f, 0, 0) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7])) //Right
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0.01f, 0, 0) * gameTime.ElapsedGameTime.Milliseconds);
                    }

                    // ONLY MAKE MOVING BLOCK SOUND WHEN PLAYER IS HOLDING A DIRECTIONAL KEY
                    tickCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])
                        || keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])
                        || keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])
                        || keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7]))
                    {
                        if (tickCount >= 0.65)
                        {
                            object[] parameters = { "movingBlock" };
                            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                            tickCount = 0;
                        }
                    }
                }
                else if (this.ID == "collidableHexa" && (collidee.ActorType == ActorType.MoveableBlock2 && switch2)) // ONLY HEXAHEDRON CAN MOVE BLOCKS WHEN THEY HAVE BEEN SWITCHED ON
                {
                    if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])) //Forward
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0, 0, -0.01f) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])) //Backward
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0, 0, 0.01f) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])) //Left
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(-0.01f, 0, 0) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7])) //Right
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0.01f, 0, 0) * gameTime.ElapsedGameTime.Milliseconds);
                    }

                    // ONLY MAKE MOVING BLOCK SOUND WHEN PLAYER IS HOLDING A DIRECTIONAL KEY
                    tickCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])
                        || keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])
                        || keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])
                        || keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7]))
                    {
                        if (tickCount >= 0.65)
                        {
                            object[] parameters = { "movingBlock" };
                            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                            tickCount = 0;
                        }
                    }
                }
                else if (this.ID == "collidableHexa" && (collidee.ActorType == ActorType.MoveableBlock3 && switch3)) // ONLY HEXAHEDRON CAN MOVE BLOCKS WHEN THEY HAVE BEEN SWITCHED ON
                {
                    if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])) //Forward
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0, 0, -0.01f) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])) //Backward
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0, 0, 0.01f) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])) //Left
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(-0.01f, 0, 0) * gameTime.ElapsedGameTime.Milliseconds);
                    }
                    else if (keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7])) //Right
                    {
                        (collidee as DrawnActor3D).Transform3D.TranslateBy(new Vector3(0.01f, 0, 0) * gameTime.ElapsedGameTime.Milliseconds);
                    }

                    // ONLY MAKE MOVING BLOCK SOUND WHEN PLAYER IS HOLDING A DIRECTIONAL KEY
                    tickCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (keyboardManager.IsKeyDown(moveKeys[0]) || keyboardManager.IsKeyDown(moveKeys[4])
                        || keyboardManager.IsKeyDown(moveKeys[1]) || keyboardManager.IsKeyDown(moveKeys[5])
                        || keyboardManager.IsKeyDown(moveKeys[2]) || keyboardManager.IsKeyDown(moveKeys[6])
                        || keyboardManager.IsKeyDown(moveKeys[3]) || keyboardManager.IsKeyDown(moveKeys[7]))
                    {
                        if (tickCount >= 0.65)
                        {
                            object[] parameters = { "movingBlock" };
                            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                            tickCount = 0;
                        }
                    }
                }
            }
        }
    }
}