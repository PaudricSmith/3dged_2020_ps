﻿#define DEMO

using GDGame.Controllers;
using GDGame.MyGame.Managers;
using GDLibrary;
using GDLibrary.Actors;
using GDLibrary.Containers;
using GDLibrary.Controllers;
using GDLibrary.Core.Controllers;
using GDLibrary.Core.Managers.State;
using GDLibrary.Debug;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Factories;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using GDLibrary.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace GDGame
{
    public class Main : Game
    {
        #region Fields

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private CameraManager<Camera3D> cameraManager;
        private ObjectManager objectManager;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private RenderManager renderManager;
        private UIManager uiManager;
        private MyMenuManager menuManager;
        private SoundManager soundManager;
        private MyGameStateManager myGameStateManager;

        private Transform3DCurve curveA;
        private Vector3 finalCurveALocation;

        private CollidablePlayerObject tetrahedron;
        private CollidablePlayerObject hexahedron;
        private CollidablePlayerObject octahedron;

        private PrimitiveObject propTetrahedron;
        private PrimitiveObject propHexahedron;
        private PrimitiveObject propOctahedron;

        private UITextObject timeTextObject;
        private UITextObject timeResultText;
        private UITextObject resultTimeText;

        private float timerCount = 0;
        private bool startTimer = false;
        private bool levelOneStarted = false;
        private float finalTime = 0;

        //used to process and deliver events received from publishers
        private EventDispatcher eventDispatcher;

        //store useful game resources (e.g. effects, models, rails and curves)
        private Dictionary<string, BasicEffect> effectDictionary;

        //use ContentDictionary to store assets (i.e. file content) that need the Content.Load() method to be called
        private ContentDictionary<Texture2D> textureDictionary;

        private ContentDictionary<SpriteFont> fontDictionary;
        private ContentDictionary<Model> modelDictionary;

        //use normal Dictionary to store objects that do NOT need the Content.Load() method to be called (i.e. the object is not based on an asset file)
        private Dictionary<string, Transform3DCurve> transform3DCurveDictionary;

        //stores the rails used by the camera
        private Dictionary<string, RailParameters> railDictionary;

        //stores the archetypal primitive objects (used in Main and LevelLoader)
        private Dictionary<string, PrimitiveObject> archetypeDictionary;

        //defines centre point for the mouse i.e. (w/2, h/2)
        private Vector2 screenCentre;

        #endregion Fields

        #region Constructors

        public Main()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            _graphics.IsFullScreen = true;
            //IsMouseVisible = false;
            
        }

        #endregion Constructors

        #region Debug
#if DEBUG

        private void InitDebug()
        {
            InitDebugInfo(false);
            bool bShowCDCRSurfaces = false;
            bool bShowZones = false;
            InitializeDebugCollisionSkinInfo(bShowCDCRSurfaces, bShowZones, Color.White);
        }

        private void InitializeDebugCollisionSkinInfo(bool bShowCDCRSurfaces, bool bShowZones, Color boundingBoxColor)
        {
            //draws CDCR surfaces for boxes and spheres
            PrimitiveDebugDrawer primitiveDebugDrawer =
                new PrimitiveDebugDrawer(this, StatusType.Drawn | StatusType.Update,
                objectManager, cameraManager,
                bShowCDCRSurfaces, bShowZones);

            primitiveDebugDrawer.DrawOrder = 5;
            BoundingBoxDrawer.BoundingBoxColor = boundingBoxColor;

            Components.Add(primitiveDebugDrawer);
        }


        private void InitDebugInfo(bool bEnable)
        {
            if (bEnable)
            {
                //create the debug drawer to draw debug info
                DebugDrawer debugInfoDrawer = new DebugDrawer(this, _spriteBatch,
                    Content.Load<SpriteFont>("Assets/Fonts/debug"),
                    cameraManager, objectManager);

                //set the debug drawer to be drawn AFTER the object manager to the screen
                debugInfoDrawer.DrawOrder = 0;

                //add the debug drawer to the component list so that it will have its Update/Draw methods called each cycle.
                Components.Add(debugInfoDrawer);
            }
        }

#endif
        #endregion Debug

        #region Load - Assets

        private void LoadSounds()
        {
            soundManager.Add(new GDLibrary.Managers.Cue("smokealarm", Content.Load<SoundEffect>("Assets/Audio/Effects/smokealarm1"), 
                SoundCategoryType.Alarm, new Vector3(1, 0, 0), false));

            // MAIN MENU THEME
            this.soundManager.Add(new GDLibrary.Managers.Cue("menuTheme", Content.Load<SoundEffect>("Assets/Audio/Music/menuTheme"), 
                SoundCategoryType.BackgroundMusic, new Vector3(0.5f, 0, 0), true));
            // TETRAHEDRON THEME
            this.soundManager.Add(new GDLibrary.Managers.Cue("tetTheme", Content.Load<SoundEffect>("Assets/Audio/Music/tetTheme"),
                SoundCategoryType.BackgroundMusic, new Vector3(0.5f, 0, 0), true));
            // HEXAHEDRON THEME
            this.soundManager.Add(new GDLibrary.Managers.Cue("hexTheme", Content.Load<SoundEffect>("Assets/Audio/Music/hexTheme"),
                SoundCategoryType.BackgroundMusic, new Vector3(0.5f, 0, 0), true));
            // HEXAHEDRON THEME 2
            this.soundManager.Add(new GDLibrary.Managers.Cue("hexTheme2", Content.Load<SoundEffect>("Assets/Audio/Music/hexTheme2"),
                SoundCategoryType.BackgroundMusic, new Vector3(0.5f, 0, 0), true));
            // OCTAHEDRON THEME
            this.soundManager.Add(new GDLibrary.Managers.Cue("octTheme", Content.Load<SoundEffect>("Assets/Audio/Music/octTheme"),
                SoundCategoryType.BackgroundMusic, new Vector3(0.5f, 0, 0), true));

            // END ZONE SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("endZoneTheme", Content.Load<SoundEffect>("Assets/Audio/Music/goal"),
                SoundCategoryType.SFX, new Vector3(0.5f, 0, 0), false));
            // MOVING BLOCK SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("movingBlock", Content.Load<SoundEffect>("Assets/Audio/Effects/movingBlock"),
                SoundCategoryType.SFX, new Vector3(0.5f, 0, 0), false));
            // TET ARRIVE SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("tetArrive", Content.Load<SoundEffect>("Assets/Audio/Effects/tetArrive"),
                SoundCategoryType.SFX, new Vector3(0.3f, 0, 0), false));
            // TET TO HEX MORPH SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("tetToHex", Content.Load<SoundEffect>("Assets/Audio/Effects/tetToHex"),
                SoundCategoryType.SFX, new Vector3(0.3f, 0, 0), false));
            // HEX TO OCT MORPH SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("hexToOcta", Content.Load<SoundEffect>("Assets/Audio/Effects/hexToOcta"),
                SoundCategoryType.SFX, new Vector3(0.1f, 0, 0), false));
            // SWITCH SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("switch", Content.Load<SoundEffect>("Assets/Audio/Effects/switch"),
                SoundCategoryType.SFX, new Vector3(0.6f, 0, 0), false));
        }

        private void LoadEffects()
        {
            //to do...
            BasicEffect effect = null;

            //used for unlit primitives with a texture (e.g. textured quad of skybox)
            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.VertexColorEnabled = true; //otherwise we wont see RGB
            effect.TextureEnabled = true;
            effectDictionary.Add(GameConstants.Effect_UnlitTextured, effect);

            //used for wireframe primitives with no lighting and no texture (e.g. origin helper)
            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.VertexColorEnabled = true;
            effectDictionary.Add(GameConstants.Effect_UnlitWireframe, effect);

            //to do...add a new effect to draw a lit textured surface (e.g. a lit pyramid)
            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.TextureEnabled = true;
            effect.LightingEnabled = true; //redundant?
            effect.PreferPerPixelLighting = true; //cost GPU cycles
            effect.EnableDefaultLighting();
         
            //change lighting position, direction and color

            effectDictionary.Add(GameConstants.Effect_LitTextured, effect);
        }

        private void LoadTextures()
        {
            //level 1 where each image 1_1, 1_2 is a different Y-axis height specificied when we use the level loader
            textureDictionary.Load("Assets/Textures/Level/level1_1");
            textureDictionary.Load("Assets/Textures/Level/level1_2");
            //add more levels here...

            //sky
            textureDictionary.Load("Assets/Textures/Skybox/back");
            textureDictionary.Load("Assets/Textures/Skybox/left");
            textureDictionary.Load("Assets/Textures/Skybox/right");
            textureDictionary.Load("Assets/Textures/Skybox/front");
            textureDictionary.Load("Assets/Textures/Skybox/sky");
            textureDictionary.Load("Assets/Textures/Foliage/Ground/desertGround");
            textureDictionary.Load("Assets/Textures/Foliage/Ground/beachGround");

            //level
            textureDictionary.Load("Assets/Textures/Level/brick");
            textureDictionary.Load("Assets/Textures/Level/tet");
            textureDictionary.Load("Assets/Textures/Level/blank");
            textureDictionary.Load("Assets/Textures/Level/endZone");
            textureDictionary.Load("Assets/Textures/Level/hex");
            textureDictionary.Load("Assets/Textures/Level/oct");
            textureDictionary.Load("Assets/Textures/Level/checkerboard");
            textureDictionary.Load("Assets/Textures/Level/moveableBrick");
            textureDictionary.Load("Assets/Textures/Level/redWallBricks");
            textureDictionary.Load("Assets/Textures/Level/switchBrick");
            textureDictionary.Load("Assets/Textures/Level/moveableRedWallBrick");
            textureDictionary.Load("Assets/Textures/Level/water");

            //ui
            textureDictionary.Load("Assets/Textures/UI/Controls/progress_white");
            textureDictionary.Load("Assets/Textures/UI/gameBorder");

            //props
            textureDictionary.Load("Assets/Textures/Props/Crates/crate1");

            //menu
            textureDictionary.Load("Assets/Textures/UI/Controls/genericbtn");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/mainmenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/menuBG");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/menuBG2");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/menuBG3");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/audiomenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/controlsmenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/exitmenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/howToPlay");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/credits");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/results");

            //ui
            textureDictionary.Load("Assets/Textures/UI/Controls/reticuleDefault");

            //add more...
        }

        private void LoadFonts()
        {
            fontDictionary.Load("Assets/Fonts/debug");
            fontDictionary.Load("Assets/Fonts/menu");
            fontDictionary.Load("Assets/Fonts/ui");
            fontDictionary.Load("Assets/Fonts/retro");
        }

        #endregion Load - Assets

        #region Initialization - Graphics, Managers, Dictionaries, Cameras, Menu, UI

        protected override void Initialize()
        {
            float worldScale = 2000;
            //set game title
            Window.Title = "Morphedronica";

            //graphic settings - see https://en.wikipedia.org/wiki/Display_resolution#/media/File:Vector_Video_Standards8.svg
            InitGraphics(1920, 1024);

            //note that we moved this from LoadContent to allow InitDebug to be called in Initialize
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            //create event dispatcher
            InitEventDispatcher();

            //managers
            InitManagers();

            //dictionaries
            InitDictionaries();

            //load from file or initialize assets, effects and vertices
            LoadEffects();
            LoadTextures();
            LoadFonts();
            LoadSounds();

            //ui
            InitUI();
            InitMenu();

            //add archetypes that can be cloned
            InitArchetypes();
            InitMovableBlocks();
            InitSwitches();

            //drawn content (collidable and noncollidable together - its simpler)
            InitLevel(worldScale);

            InitMenuTheme();

            //curves and rails used by cameras
            InitCurves();
            InitRails();

            //cameras - notice we moved the camera creation BELOW where we created the drawn content - see DriveController
            InitCameras3D();

            #region Debug
#if DEBUG
            //debug info
            InitDebug();
#endif
            #endregion Debug

            base.Initialize();
        }

        private void InitGraphics(int width, int height)
        {
            //set resolution
            _graphics.PreferredBackBufferWidth = width;
            _graphics.PreferredBackBufferHeight = height;

            //dont forget to apply resolution changes otherwise we wont see the new WxH
            _graphics.ApplyChanges();

            //set screen centre based on resolution
            screenCentre = new Vector2(width / 2, height / 2);

            //set cull mode to show front and back faces - inefficient but we will change later
            RasterizerState rs = new RasterizerState();
            rs.CullMode = CullMode.None;
            _graphics.GraphicsDevice.RasterizerState = rs;

            //we use a sampler state to set the texture address mode to solve the aliasing problem between skybox planes
            SamplerState samplerState = new SamplerState();
            samplerState.AddressU = TextureAddressMode.Clamp;
            samplerState.AddressV = TextureAddressMode.Clamp;
            _graphics.GraphicsDevice.SamplerStates[0] = samplerState;

            //set blending
            _graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;

            //set screen centre for use when centering mouse
            screenCentre = new Vector2(width / 2, height / 2);
        }

        private void InitUI()
        {
            Transform2D transform2D = null;
            Texture2D texture = null;
            SpriteFont spriteFont = null;
            Vector2 fullScreenScaleFactor = Vector2.One;

            // GAME BORDER
            texture = textureDictionary["gameBorder"];

            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);

            UITextureObject uiTextureObject = new UITextureObject("gameBorder", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.White, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 10, texture.Width, texture.Height));

            uiManager.Add(uiTextureObject);

            #region Mouse Reticule & Text
            //texture = textureDictionary["reticuleDefault"];

            //transform2D = new Transform2D(
            //    new Vector2(512, 384), //this value doesnt matter since we will recentre in UIMouseObject::Update()
            //    0,
            //     Vector2.One,
            //    new Vector2(texture.Width / 2, texture.Height / 2),
            //    new Integer2(45, 46)); //read directly from the PNG file dimensions

            //UIMouseObject uiMouseObject = new UIMouseObject("reticule", ActorType.UIMouse,
            //    StatusType.Update | StatusType.Drawn, transform2D, Color.White,
            //    SpriteEffects.None, fontDictionary["menu"],
            //    "Hello there!",
            //    new Vector2(0, -40),
            //    Color.Yellow,
            //    0.75f * Vector2.One,
            //    0,
            //    texture,
            //    new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height), //how much of source image do we want to draw?
            //    mouseManager);

            //uiManager.Add(uiMouseObject);
            #endregion Mouse Reticule & Text

            #region Progress Control Left
            texture = textureDictionary["progress_white"];

            transform2D = new Transform2D(new Vector2(512, 20),
                0,
                 Vector2.One,
                new Vector2(texture.Width / 2, texture.Height / 2),
                new Integer2(100, 100));

            uiTextureObject = new UITextureObject("progress 1", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.Yellow, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            //uiTextureObject.ControllerList.Add(new UIRotationController("rc1", ControllerType.RotationOverTime));

            //uiTextureObject.ControllerList.Add(new UIColorLerpController("clc1", ControllerType.ColorLerpOverTime,
            //    Color.White, Color.Black));

            //uiTextureObject.ControllerList.Add(new UIMouseController("moc1", ControllerType.MouseOver,
            //    this.mouseManager));

            uiTextureObject.ControllerList.Add(new UIProgressController("pc1", ControllerType.Progress, 0, 10));

            uiManager.Add(uiTextureObject);
            #endregion Progress Control Left

            #region Timer Object
            spriteFont = fontDictionary["retro"];

            string text = "TIMER";
            Vector2 originalDimensions = spriteFont.MeasureString(text);

            transform2D = new Transform2D(new Vector2(950, 120),
                0,
                Vector2.One,
                new Vector2(originalDimensions.X / 2, originalDimensions.Y / 2), //this is text???
                new Integer2(originalDimensions)); //accurate original dimensions

            UITextObject uiTextObject = new UITextObject("timer", ActorType.UIText,
                StatusType.Update | StatusType.Drawn, transform2D, new Color(0.1f, 1, 1, 1),
                0, SpriteEffects.None, text, spriteFont);

            uiManager.Add(uiTextObject);

            #endregion Timer Object

            #region Time Oject
            string time = "0.00";
            Vector2 originalDimensions2 = spriteFont.MeasureString(time);

            transform2D = new Transform2D(new Vector2(949, 150),
                0,
                Vector2.One,
                new Vector2(originalDimensions2.X / 2, originalDimensions2.Y / 2), //this is text???
                new Integer2(originalDimensions2));

            timeTextObject = new UITextObject("time", ActorType.UIText,
                StatusType.Update | StatusType.Drawn, transform2D, new Color(0.1f, 1, 1, 1),
                0, SpriteEffects.None, time, spriteFont);

            uiManager.Add(timeTextObject);
            menuManager.Add("results", timeTextObject);

            #endregion Time Oject

            #region Result Time Oject
            time = "0.00";
            originalDimensions2 = spriteFont.MeasureString(time);

            transform2D = new Transform2D(new Vector2(1050, 500),
                0,
                Vector2.One,
                new Vector2(originalDimensions2.X / 2, originalDimensions2.Y / 2), //this is text???
                new Integer2(originalDimensions2));

            resultTimeText = new UITextObject("resultTime", ActorType.UIText,
                StatusType.Off, transform2D, new Color(0.1f, 1, 1, 1),
                0, SpriteEffects.None, time, spriteFont);

            uiManager.Add(resultTimeText);
            menuManager.Add("results", resultTimeText);

            #endregion Result Time Oject

            #region Time Result text
            spriteFont = fontDictionary["retro"];

            text = "YOUR TIME - ";
            originalDimensions = spriteFont.MeasureString(text);

            transform2D = new Transform2D(new Vector2(920, 500),
                0,
                Vector2.One,
                new Vector2(originalDimensions.X / 2, originalDimensions.Y / 2), //this is text???
                new Integer2(originalDimensions)); //accurate original dimensions

            timeResultText = new UITextObject("timeResultText", ActorType.UIText,
                StatusType.Off, transform2D, new Color(0.1f, 1, 1, 1),
                0, SpriteEffects.None, text, spriteFont);

            uiManager.Add(timeResultText);
            menuManager.Add("results", timeResultText);

            #endregion Time Result text

        }

        private void InitMenu()
        {
            Texture2D texture = null;
            Transform2D transform2D = null;
            DrawnActor2D uiObject = null;
            Vector2 fullScreenScaleFactor = Vector2.One;

            #region All Menu Background Images
            //background main
            texture = textureDictionary["menuBG2"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("main_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 10, texture.Width, texture.Height));
            menuManager.Add("main", uiObject);

            //background "how to play"
            texture = textureDictionary["howToPlay"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("howToPlay_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 10, texture.Width, texture.Height));
            menuManager.Add("howToPlay", uiObject);

            //background credits
            texture = textureDictionary["credits"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("credits_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 10, texture.Width, texture.Height));
            menuManager.Add("credits", uiObject);

            //background retry
            texture = textureDictionary["menuBG2"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("main_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 10, texture.Width, texture.Height));
            menuManager.Add("retry", uiObject);

            //background retry
            texture = textureDictionary["results"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("results_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 10, texture.Width, texture.Height));
            menuManager.Add("results", uiObject);

            //_graphics.ApplyChanges();
            #endregion All Menu Background Images

            #region Main Menu Buttons
            texture = textureDictionary["genericbtn"];

            Vector2 origin = new Vector2(texture.Width / 2, texture.Height / 2);

            Integer2 imageDimensions = new Integer2(texture.Width, texture.Height);

            //play Button
            transform2D = new Transform2D(screenCentre + new Vector2(-345, -125), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("play", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "PLAY",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("main", uiObject);

            //How To Play Button
            transform2D = new Transform2D(screenCentre + new Vector2(-345, -60), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("howToPlay", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "HOW?",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("main", uiObject);

            //credits Button
            transform2D = new Transform2D(screenCentre + new Vector2(345, 105), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("credits", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "CRED",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("main", uiObject);

            //exit Button
            transform2D = new Transform2D(screenCentre + new Vector2(345, 170), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("exit", ActorType.UITextureObject,
             StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "EXIT",
             fontDictionary["retro"],
             new Vector2(1, 1),
             new Color(184, 105, 98),
             new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(136, 57, 50), new Color(191, 206, 114)));

            menuManager.Add("main", uiObject);

            // Back Button
            transform2D = new Transform2D(screenCentre + new Vector2(190, 320), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("backBtn", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "BACK",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("howToPlay", uiObject);
            menuManager.Add("credits", uiObject);
            menuManager.Add("results", uiObject);

            // Menu Button
            transform2D = new Transform2D(screenCentre + new Vector2(345, 105), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("menuBtn", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "MENU",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("retry", uiObject);

            // Retry Button
            transform2D = new Transform2D(screenCentre + new Vector2(-345, -60), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("retryBtn", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "TRY?",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("retry", uiObject);

            //continue Button
            transform2D = new Transform2D(screenCentre + new Vector2(-345, -125), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("continue", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "CONT",
                fontDictionary["retro"],
                new Vector2(1, 1),
                new Color(103, 182, 189),
                new Vector2(0, -3));

            uiObject.ControllerList.Add(new UIMouseOverController("mouseOver1", ControllerType.MouseOver,
                 mouseManager, new Color(103, 182, 189), new Color(191, 206, 114)));

            menuManager.Add("retry", uiObject);

            #endregion Main Menu Buttons

            //finally dont forget to SetScene to say which menu should be drawn/updated!
            menuManager.SetScene("main");
            
            
        }

        /// <summary>
        /// Plays the menu theme as soon as the game boots
        /// </summary>
        private void InitMenuTheme()
        {
            object[] parameters = { "menuTheme" };
            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));
        }

        private void InitEventDispatcher()
        {
            eventDispatcher = new EventDispatcher(this);
            Components.Add(eventDispatcher);
        }

        private void InitCurves()
        {
            finalCurveALocation = new Vector3(-30, 270, 10);

            //create the camera curve to be applied to the track controller
            curveA = new Transform3DCurve(CurveLoopType.Constant);

            curveA.Add(new Vector3(105, 600, -350), new Vector3(0, -1.75f, 1), Vector3.UnitY, 1000); //start
            curveA.Add(new Vector3(105, 430, -300), new Vector3(0, -1.75f, 1), Vector3.UnitY, 2000);
            curveA.Add(new Vector3(105, 370, -200), new Vector3(0, -1.75f, 1), Vector3.UnitY, 3000);
            curveA.Add(new Vector3(105, 370, -100), new Vector3(0, -1.75f, 1), Vector3.UnitY, 4000);
            curveA.Add(new Vector3(105, 370, 0), new Vector3(0, -1.75f, 1), Vector3.UnitY, 5000);
            curveA.Add(new Vector3(105, 370, 300), new Vector3(-1, -1.75f, -1), Vector3.UnitY, 6000);

            curveA.Add(new Vector3(105, 370, 300), new Vector3(0, -1.75f, -1), Vector3.UnitY, 7000);
            curveA.Add(new Vector3(105, 370, 200), new Vector3(0, -1.75f, -1), Vector3.UnitY, 8000);
            curveA.Add(new Vector3(105, 370, 100), new Vector3(0, -1.75f, -1), Vector3.UnitY, 9000);
            curveA.Add(new Vector3(50, 320, 0), new Vector3(0, -1.75f, -1), Vector3.UnitY, 10000);
            curveA.Add(finalCurveALocation, new Vector3(0, -1.75f, -1), Vector3.UnitY, 11000);

            //add to the dictionary
            transform3DCurveDictionary.Add("levelTour1", curveA);
        }

        private void InitRails()
        {
            //create the track to be applied to the non-collidable track camera 1
            railDictionary.Add("rail1", new RailParameters("rail1 - parallel to z-axis", new Vector3(20, 10, 50), new Vector3(20, 10, -50)));
        }

        private void InitDictionaries()
        {
            //stores effects
            effectDictionary = new Dictionary<string, BasicEffect>();

            //stores textures, fonts & models
            modelDictionary = new ContentDictionary<Model>("models", Content);
            textureDictionary = new ContentDictionary<Texture2D>("textures", Content);
            fontDictionary = new ContentDictionary<SpriteFont>("fonts", Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            transform3DCurveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails - store rails used by cameras
            railDictionary = new Dictionary<string, RailParameters>();

            //used to store archetypes for primitives in the game
            archetypeDictionary = new Dictionary<string, PrimitiveObject>();

        }

        private void InitManagers()
        {
            //physics and CD-CR (moved to top because MouseManager is dependent)
            //to do - replace with simplified CDCR

            //camera
            cameraManager = new CameraManager<Camera3D>(this, StatusType.Off);
            Components.Add(cameraManager);

            //keyboard
            keyboardManager = new KeyboardManager(this);
            Components.Add(keyboardManager);

            //mouse
            mouseManager = new MouseManager(this, true, screenCentre);
            Components.Add(mouseManager);

            //object
            objectManager = new ObjectManager(this, StatusType.Off, 6, 10);
            Components.Add(objectManager);

            //render
            renderManager = new RenderManager(this, StatusType.Drawn, ScreenLayoutType.Single,
                objectManager, cameraManager);
            Components.Add(renderManager);

            //add in-game ui
            uiManager = new UIManager(this, StatusType.Off, _spriteBatch, 10);
            uiManager.DrawOrder = 4;
            Components.Add(uiManager);

            //add menu
            menuManager = new MyMenuManager(this, StatusType.Update | StatusType.Drawn, _spriteBatch,
                mouseManager, keyboardManager);
            menuManager.DrawOrder = 5; //highest number of all drawable managers since we want it drawn on top!
            Components.Add(menuManager);

            //sound
            soundManager = new SoundManager(this, StatusType.Update);
            Components.Add(soundManager);

            //gamestate manager
            myGameStateManager = new MyGameStateManager(this, StatusType.Update, objectManager, menuManager, cameraManager);
            Components.Add(myGameStateManager);
        }

        private void InitCameras3D()
        {
            Transform3D transform3D = null;
            Camera3D camera3D = null;
            Viewport viewPort = new Viewport(0, 0, 1920, 1024);

            #region Noncollidable Camera - Curve3D

            //notice that it doesnt matter what translation, look, and up are since curve will set these
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.Zero);

            camera3D = new Camera3D(GameConstants.Camera_NonCollidableCurveMainArena,
              ActorType.Camera3D, StatusType.Update, transform3D,
                        ProjectionParameters.StandardDeepSixteenTen, viewPort);

            camera3D.ControllerList.Add(
                new Curve3DController(GameConstants.Controllers_NonCollidableCurveMainArena,
                ControllerType.Curve,
                        transform3DCurveDictionary["levelTour1"])); //use the curve dictionary to retrieve a transform3DCurve by id

            // cameraManager.ActiveCameraIndex = 0;
            cameraManager.Add(camera3D);

            #endregion Noncollidable Camera - Curve3D

            #region Collidable Tetra Camera - Fixed 

            transform3D = new Transform3D(new Vector3(-29.9f, 270, 10),
                new Vector3(0, -1.75f, -1), new Vector3(0, 1, 0));

            camera3D = new Camera3D(GameConstants.Camera_CollidableTetraFixed,
                ActorType.Camera3D, StatusType.Off, transform3D,
                ProjectionParameters.StandardDeepSixteenTen,
                viewPort);

            //attach a controller
            camera3D.ControllerList.Add(new ThirdPersonController(
                GameConstants.Controllers_CollidableThirdPerson,
                ControllerType.ThirdPerson,
                tetrahedron,
                120,
                270,
                1f,
                mouseManager));

            // cameraManager.ActiveCameraIndex = 1;
            cameraManager.Add(camera3D);

            #endregion Collidable Tetra Camera - Fixed

            #region Collidable Hexa Camera - Fixed 

            //transform3D = new Transform3D(new Vector3(237, 21, 140),
            //    new Vector3(0, -1.75f, -1), new Vector3(0, 1, 0));

            //camera3D = new Camera3D(GameConstants.Camera_CollidableTetraFixed,
            //    ActorType.Camera3D, StatusType.Off, transform3D,
            //    ProjectionParameters.StandardDeepSixteenTen,
            //    viewPort);

            ////attach a controller
            //camera3D.ControllerList.Add(new TetraThirdPersonController(
            //    GameConstants.Controllers_CollidableThirdPerson,
            //    ControllerType.ThirdPerson,
            //    hexahedron,
            //    120,
            //    270,
            //    1f,
            //    mouseManager));

            //// cameraManager.ActiveCameraIndex = 1;
            //cameraManager.Add(camera3D);

            #endregion Collidable Hexa Camera - Fixed

            #region Collidable Octa Camera - Fixed 

            //transform3D = new Transform3D(new Vector3(-30, 21, 140),
            //    new Vector3(0, -1.75f, -1), new Vector3(0, 1, 0));

            //camera3D = new Camera3D(GameConstants.Camera_CollidableTetraFixed,
            //    ActorType.Camera3D, StatusType.Off, transform3D,
            //    ProjectionParameters.StandardDeepSixteenTen,
            //    viewPort);

            ////attach a controller
            //camera3D.ControllerList.Add(new TetraThirdPersonController(
            //    GameConstants.Controllers_CollidableThirdPerson,
            //    ControllerType.ThirdPerson,
            //    octahedron,
            //    120,
            //    270,
            //    1f,
            //    mouseManager));

            //// cameraManager.ActiveCameraIndex = 1;
            //cameraManager.Add(camera3D);

            #endregion Collidable Octa Camera - Fixed

            #region Noncollidable Camera - Flight

            transform3D = new Transform3D(new Vector3(10, 10, 20),
                new Vector3(0, 0, -1), Vector3.UnitY);

            camera3D = new Camera3D(GameConstants.Camera_NonCollidableFlight,
                ActorType.Camera3D, StatusType.Update, transform3D,
                ProjectionParameters.StandardDeepSixteenTen, new Viewport(0, 0, 1920, 1024));

            //attach a controller
            camera3D.ControllerList.Add(new FlightCameraController(
                GameConstants.Controllers_NonCollidableFlight, ControllerType.FlightCamera,
                keyboardManager, mouseManager, null,
                GameConstants.CameraMoveKeys,
                10 * GameConstants.flightMoveSpeed,
                10 * GameConstants.flightStrafeSpeed,
                GameConstants.flightRotateSpeed));

            // cameraManager.ActiveCameraIndex = 2;
            cameraManager.Add(camera3D);

            #endregion Noncollidable Camera - Flight

            cameraManager.ActiveCameraIndex = 0; //0, 1, 2, 3
        }

        #endregion Initialization - Graphics, Managers, Dictionaries, Cameras, Menu, UI

        #region Initialization - Vertices, Archetypes, Helpers, Drawn Content(e.g. Skybox)

        /// <summary>
        /// Creates archetypes used in the game.
        ///
        /// What are the steps required to add a new primitive?
        ///    1. In the VertexFactory add a function to return Vertices[]
        ///    2. Add a new BasicEffect IFF this primitive cannot use existing effects(e.g.wireframe, unlit textured)
        ///    3. Add the new effect to effectDictionary
        ///    4. Create archetypal PrimitiveObject.
        ///    5. Add archetypal object to archetypeDictionary
        ///    6. Clone archetype, change its properties (transform, texture, color, alpha, ID) and add manually to the objectmanager or you can use LevelLoader.
        /// </summary>
        private void InitArchetypes() //formerly InitTexturedQuad
        {
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            IVertexData vertexData = null;
            PrimitiveType primitiveType;
            int primitiveCount;
            PrimitiveObject primitiveObject;

            ICollisionPrimitive boxPrim = null;
            CollidablePrimitiveObject collidablePrimitiveObject = null;

            #region Lit Textured Tetrahedron

            /*********** Transform, Vertices and VertexData ***********/
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["tet"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            VertexPositionNormalTexture[] vertices = VertexFactory.GetVerticesPositionNormalTexturedTetrahedron(out primitiveType, out primitiveCount);

            //analog of the Model class in G-CA(i.e.it holdes vertices and type, count)
            vertexData = new VertexData<VertexPositionNormalTexture>(vertices, primitiveType, primitiveCount);

            /*********** PrimitiveObject ***********/
            //now we use the "FBX" file(our vertexdata) and make a PrimitiveObject
            primitiveObject = new PrimitiveObject(
                GameConstants.Primitive_LitTexturedTetrahedron,
                ActorType.CollidableDecorator, 
                StatusType.Drawn | StatusType.Update, 
                transform3D, 
                effectParameters,
                vertexData);

            archetypeDictionary.Add(primitiveObject.ID, primitiveObject);

            #endregion Lit Textured Tetrahedron

            #region Unlit Textured Quad
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_UnlitTextured], textureDictionary["blank"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertexData = new VertexData<VertexPositionColorTexture>(VertexFactory.GetTextureQuadVertices(out primitiveType, out primitiveCount), primitiveType, primitiveCount);

            archetypeDictionary.Add(GameConstants.Primitive_UnlitTexturedQuad,
                new CollidablePrimitiveObject(GameConstants.Primitive_UnlitTexturedQuad,
                ActorType.Decorator,
                StatusType.Drawn,
                transform3D, effectParameters, vertexData, boxPrim, objectManager));

            #endregion Unlit Textured Quad

            #region Lit Textured Quad

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["blank"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertexData = new VertexData<VertexPositionColorTexture>(VertexFactory.GetVerticesPositionColorTextureQuad(5, out primitiveType, out primitiveCount), primitiveType, primitiveCount);

            archetypeDictionary.Add(GameConstants.Primitive_LitTexturedQuad,
                new CollidablePrimitiveObject(GameConstants.Primitive_LitTexturedQuad,
                ActorType.Decorator,
                StatusType.Drawn,
                transform3D, effectParameters, vertexData, boxPrim, objectManager));

            #endregion Lit Textured Quad

            #region Unlit Origin Helper
            transform3D = new Transform3D(new Vector3(0, 20, 0),
                     Vector3.Zero, new Vector3(10, 10, 10),
                     Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitWireframe],
                null, Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertexData = new VertexData<VertexPositionColor>(VertexFactory.GetVerticesPositionColorOriginHelper(
                                    out primitiveType, out primitiveCount),
                                    primitiveType, primitiveCount);

            archetypeDictionary.Add(GameConstants.Primitive_WireframeOriginHelper,
                new CollidablePrimitiveObject(GameConstants.Primitive_WireframeOriginHelper,
                ActorType.Helper,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters, vertexData, boxPrim, objectManager));

            #endregion Unlit Origin Helper

            #region Lit Textured Cube

            /*********** Transform, Vertices and VertexData ***********/

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["redWallBricks"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            //Buffered Vertex Data
            BufferedVertexData<VertexPositionNormalTexture> bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            /*********** PrimitiveObject ***********/
            //now we use the "FBX" file(our vertexdata) and make a PrimitiveObject
            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedCube,
                ActorType.CollidableDecorator, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured Cube

            #region Lit Textured Water

            /*********** Transform, Vertices and VertexData ***********/

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["water"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            //Buffered Vertex Data
            bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            /*********** PrimitiveObject ***********/
            //now we use the "FBX" file(our vertexdata) and make a PrimitiveObject
            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedWater,
                ActorType.CollidableDecorator, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured Water

            #region Lit Textured Octahedron

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["oct"], Color.White, 1);

            //boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedOctahedron(out primitiveType,
                out primitiveCount);

            vertexData = new VertexData<VertexPositionNormalTexture>(vertices,
                primitiveType, primitiveCount);

            /*********** PrimitiveObject ***********/
            //now we use the "FBX" file (our vertexdata) and make a PrimitiveObject
            primitiveObject = new PrimitiveObject(
                GameConstants.Primitive_LitTexturedOctahedron,
                ActorType.CollidableDecorator, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, 
                effectParameters,
                vertexData);

            archetypeDictionary.Add(primitiveObject.ID, primitiveObject);

            #endregion Lit Textured Octahedron

        }

        private void InitMovableBlocks()
        {
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            PrimitiveType primitiveType;
            ICollisionPrimitive boxPrim = null;
            CollidablePrimitiveObject collidablePrimitiveObject = null;
            int primitiveCount;

            #region Lit Textured Moveable Block1

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["moveableRedWallBrick"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            VertexPositionNormalTexture[] vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            //Buffered Vertex Data
            BufferedVertexData<VertexPositionNormalTexture> bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            /*********** PrimitiveObject ***********/
            //now we use the "FBX" file(our vertexdata) and make a PrimitiveObject
            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedMoveableBlock1,
                ActorType.MoveableBlock1, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured Moveable Block1

            #region Lit Textured Moveable Block2

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["moveableRedWallBrick"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedMoveableBlock2,
                ActorType.MoveableBlock2, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured Moveable Block2

            #region Lit Textured Moveable Block3

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["moveableRedWallBrick"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedMoveableBlock3,
                ActorType.MoveableBlock3, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured Moveable Block3
        }

        private void InitSwitches()
        {
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            PrimitiveType primitiveType;
            ICollisionPrimitive boxPrim = null;
            CollidablePrimitiveObject collidablePrimitiveObject = null;
            int primitiveCount;

            #region Lit Textured switch1

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["switchBrick"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            VertexPositionNormalTexture[] vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            BufferedVertexData<VertexPositionNormalTexture> bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedSwitch1,
                ActorType.Switch1, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured switch1

            #region Lit Textured switch2

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["switchBrick"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedSwitch2,
                ActorType.Switch2, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured switch2

            #region Lit Textured switch3

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["switchBrick"], Color.White, 1);

            boxPrim = new BoxCollisionPrimitive(transform3D);

            vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
                (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedSwitch3,
                ActorType.Switch3, //we could specify any time e.g. Pickup
                StatusType.Drawn | StatusType.Update,
                transform3D, effectParameters,
                bufferedVertexData, boxPrim, this.objectManager);

            archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            #endregion Lit Textured switch3
        }
        private void InitLevel(float worldScale)//, List<string> levelNames)
        {
            //remove any old content (e.g. on restart or next level)
            objectManager.Clear();

            /************ Non-collidable ************/
            //adds origin helper etc
            InitHelpers();

            //add skybox
            InitSkybox(worldScale);

            //add grass plane
            InitGround(worldScale);

            

            /************ Collidable ************/

            //InitCollidableProps();

            InitCollidableZones();

            InitializeCollidablePlayer();

            //pyramids
            InitDecorators();

            // ***************************************************************************************************************************************************
            // ***************************************************************************************************************************************************
            // ************************** COMMENTED THIS OUT FOR LATER *******************************************************************************************

            //CollidablePrimitiveObject collPrimObj = new CollidablePrimitiveObject("id",
            //    ActorType.CollidableDecorator, StatusType.Drawn, transform3D,
            //    effectParameters, vertexData, boxPrim, this.objectManager);

            // ***************************************************************************************************************************************************
            // ***************************************************************************************************************************************************
            // ***************************************************************************************************************************************************



            /************ Level-loader (can be collidable or non-collidable) ************/

            LevelLoader<PrimitiveObject> levelLoader = new LevelLoader<PrimitiveObject>(
                this.archetypeDictionary, this.textureDictionary);
            List<DrawnActor3D> actorList = null;

            //add level1_1 contents
            actorList = levelLoader.Load(
                this.textureDictionary["level1_1"],
                                10,     //number of in-world x-units represented by 1 pixel in image
                                10,     //number of in-world z-units represented by 1 pixel in image
                                20,     //y-axis height offset
                                new Vector3(-50, 0, -150) //offset to move all new objects by
                                );
            this.objectManager.Add(actorList);

            //clear the list otherwise when we add level1_2 we would re-add level1_1 objects to object manager
            //actorList.Clear();

            //add level1_2 contents
            //actorList = levelLoader.Load(
            // this.textureDictionary["level1_2"],
            //                 10,     //number of in-world x-units represented by 1 pixel in image
            //                 10,     //number of in-world z-units represented by 1 pixel in image
            //                 40,     //y-axis height offset
            //                 new Vector3(-50, 0, -150) //offset to move all new objects by
            //                 );
            //this.objectManager.Add(actorList);
        }

        private void InitCollidableProps()
        {
            //Transform3D transform3D = null;
            //EffectParameters effectParameters = null;
            //ICollisionPrimitive collisionPrimitive = null;
            //CollidablePrimitiveObject collidablePrimitiveObject = null;
            //PrimitiveType primitiveType;
            //int primitiveCount;

            ///************************* Box Collision Primitive  *************************/

            //transform3D = new Transform3D(new Vector3(-50, 20, 160), Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);

            ////a unique effectparameters instance for each box in case we want different color, texture, alpha
            //effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured], textureDictionary["redWallBricks"], Color.White, 1);

            //VertexPositionNormalTexture[] vertices = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType, out primitiveCount);

            ////Buffered Vertex Data
            //BufferedVertexData<VertexPositionNormalTexture> bufferedVertexData = new BufferedVertexData<VertexPositionNormalTexture>
            //    (_graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            ////make the collision primitive - changed slightly to no longer need transform
            //collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            ////make a collidable object and pass in the primitive
            //collidablePrimitiveObject = new CollidablePrimitiveObject(
            //    GameConstants.Primitive_LitTexturedCube,
            //    ActorType.CollidableDecorator,  //this is important as it will determine how we filter collisions in our collidable player CDCR code
            //    StatusType.Drawn | StatusType.Update,
            //    transform3D,
            //    effectParameters,
            //    bufferedVertexData,
            //    collisionPrimitive, objectManager);

            //objectManager.Add(collidablePrimitiveObject);

            //add to the archetype dictionary
            //archetypeDictionary.Add(collidablePrimitiveObject.ID, collidablePrimitiveObject);

            // IF YOU WANT TO SET COLLIDABLE CUBE MANUALLY ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

            //transform3D = new Transform3D(new Vector3(0, 20, 0), Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            //collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            ////change it a bit
            //collidablePrimitiveObject.ID = "block1";
            //collidablePrimitiveObject.Transform3D = transform3D;
            //collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;
            //collidablePrimitiveObject.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);

            //objectManager.Add(collidablePrimitiveObject);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //collidablePrimitiveObject = collidablePrimitiveObject.Clone() as CollidablePrimitiveObject;

            //transform3D = new Transform3D(new Vector3(0, 20, 10), Vector3.Zero, 10 * Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            //collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            ////change it a bit
            //collidablePrimitiveObject.ID = "block2";
            //collidablePrimitiveObject.Transform3D = transform3D;
            //collidablePrimitiveObject.CollisionPrimitive = collisionPrimitive;
            //collidablePrimitiveObject.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);

            //objectManager.Add(collidablePrimitiveObject);

        }

        private void InitCollidableZones()
        {
            Transform3D transform3D = null;
            ICollisionPrimitive collisionPrimitive = null;
            CollidableZoneObject collidableZoneObject = null;

            transform3D = new Transform3D(new Vector3(100, 20, 140),
                Vector3.Zero, new Vector3(10, 10, 10), Vector3.UnitZ, Vector3.UnitY);

            //make the collision primitive - changed slightly to no longer need transform
            collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            collidableZoneObject = new CollidableZoneObject("endZone", ActorType.CollidableZone,
                StatusType.Drawn | StatusType.Update,
                transform3D,
                collisionPrimitive);

            objectManager.Add(collidableZoneObject);
        }

        private void InitializeCollidablePlayer()
        {
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            IVertexData vertexData = null;
            ICollisionPrimitive collisionPrimitive = null;
            PrimitiveType primitiveType;
            int primitiveCount;
            float tetrahedronSpeedMultiplier = 0.75f;
            float hexahedronSpeedMultiplier = 0.25f;
            float octahedronSpeedMultiplier = 0.5f;

            #region tetrahedron player

            //set the position
            transform3D = new Transform3D(new Vector3(-35, 21, -135), new Vector3(0, 90, 0), new Vector3(10, 10, 10),
                -Vector3.UnitZ, Vector3.UnitY);

            //a unique effectparameters instance for each box in case we want different color, texture, alpha
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured],
                textureDictionary["tet"], Color.White, 1);

            //get the vertex data object            
            vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedTetrahedron(
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

            //make a CDCR surface - sphere or box, its up to you - you dont need to pass transform to either primitive anymore
            collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            //if we make this a field then we can pass to the 3rd person camera controller
            tetrahedron = new CollidablePlayerObject("collidableTetra",
                   //this is important as it will determine how we filter collisions in our collidable player CDCR code
                   ActorType.TetraPlayer,
                   StatusType.Off,
                   transform3D,
                   effectParameters,
                   vertexData,
                   collisionPrimitive,
                   objectManager,
                   this,
                   myGameStateManager,
                   GameConstants.KeysOne,
                   GameConstants.FixedMoveSpeed * tetrahedronSpeedMultiplier,
                   GameConstants.FixedStrafeSpeed * tetrahedronSpeedMultiplier,
                   keyboardManager);

            objectManager.Add(tetrahedron);

            #endregion tetrahedron player

            #region hexahedron player

            //set the position
            transform3D = new Transform3D(new Vector3(237, 21, 140), new Vector3(0, 0, 0), new Vector3(10, 10, 10),
                -Vector3.UnitZ, Vector3.UnitY);

            //a unique effectparameters instance for each box in case we want different color, texture, alpha
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured],
                textureDictionary["hex"], Color.White, 1);

            //get the vertex data object            
            vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedCube(1,
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

            //make a CDCR surface - sphere or box, its up to you - you dont need to pass transform to either primitive anymore
            collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            //if we make this a field then we can pass to the 3rd person camera controller
            hexahedron = new CollidablePlayerObject("collidableHexa",
                   //this is important as it will determine how we filter collisions in our collidable player CDCR code
                   ActorType.HexaPlayer,
                   StatusType.Off,
                   transform3D,
                   effectParameters,
                   vertexData,
                   collisionPrimitive,
                   objectManager,
                   this,
                   myGameStateManager,
                   GameConstants.KeysOne,
                   GameConstants.FixedMoveSpeed * hexahedronSpeedMultiplier,
                   GameConstants.FixedStrafeSpeed * hexahedronSpeedMultiplier,
                   keyboardManager);

            objectManager.Add(hexahedron);

            #endregion hexahedron player

            #region octahedron player

            //set the position
            transform3D = new Transform3D(new Vector3(-30, 21, 140), new Vector3(0, 0, 0), new Vector3(10, 10, 10),
                -Vector3.UnitZ, Vector3.UnitY);

            //a unique effectparameters instance for each box in case we want different color, texture, alpha
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured],
                textureDictionary["oct"], Color.White, 1);

            //get the vertex data object            
            vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedOctahedron(
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

            //make a CDCR surface - sphere or box, its up to you - you dont need to pass transform to either primitive anymore
            collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            //if we make this a field then we can pass to the 3rd person camera controller
            octahedron = new CollidablePlayerObject("collidableOcta",
                   //this is important as it will determine how we filter collisions in our collidable player CDCR code
                   ActorType.OctaPlayer,
                   StatusType.Off,
                   transform3D,
                   effectParameters,
                   vertexData,
                   collisionPrimitive,
                   objectManager,
                   this,
                   myGameStateManager,
                   GameConstants.KeysOne,
                   GameConstants.FixedMoveSpeed * octahedronSpeedMultiplier,
                   GameConstants.FixedStrafeSpeed * octahedronSpeedMultiplier,
                   keyboardManager);

            objectManager.Add(octahedron);

            #endregion octahedron player
        }


        /// <summary>
        /// Demos how we can clone an archetype and manually add to the object manager.
        /// </summary>
        private void InitDecorators()
        {
            #region tetrahedron prop

            //clone the archetypal tetrahedron
            propTetrahedron = archetypeDictionary[GameConstants.Primitive_LitTexturedTetrahedron].Clone() as PrimitiveObject;

            //change it a bit
            propTetrahedron.ID = "tetrahedron";
            propTetrahedron.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
            propTetrahedron.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            propTetrahedron.Transform3D.Translation = new Vector3(-35, 16, -135);
            propTetrahedron.EffectParameters.Alpha = 1f;

            propTetrahedron.ControllerList.Add(
                new RotationController("rot controller1", ControllerType.RotationOverTime,
                1, new Vector3(0, 1, 0)));

            //finally add it into the objectmanager after SIX(!) steps
            objectManager.Add(propTetrahedron);

            #endregion tetrahedron prop

            #region hexahedron prop

            //clone the archetypal cube
            propHexahedron = archetypeDictionary[GameConstants.Primitive_LitTexturedCube].Clone() as PrimitiveObject;

            //change it a bit
            propHexahedron.ID = "cube";
            propHexahedron.EffectParameters.Texture = textureDictionary["hex"];
            propHexahedron.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
            propHexahedron.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);
            propHexahedron.Transform3D.Translation = new Vector3(237, 21, 140);
            propHexahedron.EffectParameters.Alpha = 1f;

            //lets add a rotation controller so we can see all sides easily
            propHexahedron.ControllerList.Add(
                new RotationController("rot controller1", ControllerType.RotationOverTime,
                1, new Vector3(0, 1, 0)));

            //finally add it into the objectmanager after SIX(!) steps
            objectManager.Add(propHexahedron);

            #endregion hexahedron prop

            #region octahedron prop

            //clone the archetypal octahedron
            propOctahedron = archetypeDictionary[GameConstants.Primitive_LitTexturedOctahedron].Clone() as PrimitiveObject;

            //change it a bit
            propOctahedron.ID = "octahedron";
            propOctahedron.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
            propOctahedron.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);
            propOctahedron.Transform3D.Translation = new Vector3(-30, 21, 140);
            propOctahedron.EffectParameters.Alpha = 1f;

            //lets add a rotation controller so we can see all sides easily
            propOctahedron.ControllerList.Add(
                new RotationController("rot controller1", ControllerType.RotationOverTime,
                1, new Vector3(0, 1, 0)));

            //finally add it into the objectmanager after SIX(!) steps
            objectManager.Add(propOctahedron);

            #endregion octahedron prop

            #region end zone plate

            PrimitiveObject drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;

            drawnActor3D.ID = "endZonePlate";
            drawnActor3D.ActorType = ActorType.Decorator;
            drawnActor3D.EffectParameters.Texture = textureDictionary["endZone"];
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(-90, 0, 0);
            drawnActor3D.Transform3D.Scale = 10 * Vector3.One;
            drawnActor3D.Transform3D.Translation = new Vector3(100, 15, 140);

            objectManager.Add(drawnActor3D);

            #endregion end zone plate

        }

        private void InitHelpers()
        {
            ////clone the archetype
            //PrimitiveObject originHelper = archetypeDictionary[GameConstants.Primitive_WireframeOriginHelper].Clone() as PrimitiveObject;
            ////add to the dictionary
            //objectManager.Add(originHelper);
        }

        private void InitGround(float worldScale)
        {
            PrimitiveObject drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Ground;
            drawnActor3D.EffectParameters.Texture = textureDictionary["beachGround"];
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(-90, 0, 0);
            drawnActor3D.Transform3D.Scale = 320 * Vector3.One;
            drawnActor3D.Transform3D.Translation = new Vector3(105, 14.7f, 5);

            objectManager.Add(drawnActor3D);
        }

        private void InitSkybox(float worldScale)
        {
            //PrimitiveObject drawnActor3D = null;

            ////back
            //drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            //drawnActor3D.ActorType = ActorType.Sky;

            ////  primitiveObject.StatusType = StatusType.Off; //Experiment of the effect of StatusType
            //drawnActor3D.ID = "sky back";
            //drawnActor3D.EffectParameters.Texture = textureDictionary["back"]; ;
            //drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            //drawnActor3D.Transform3D.Translation = new Vector3(0, 0, -worldScale / 2.0f);
            //objectManager.Add(drawnActor3D);

            ////left
            //drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            //drawnActor3D.ActorType = ActorType.Sky;
            //drawnActor3D.ID = "left back";
            //drawnActor3D.EffectParameters.Texture = textureDictionary["left"]; ;
            //drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            //drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            //drawnActor3D.Transform3D.Translation = new Vector3(-worldScale / 2.0f, 0, 0);
            //objectManager.Add(drawnActor3D);

            ////right
            //drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            //drawnActor3D.ActorType = ActorType.Sky;
            //drawnActor3D.ID = "sky right";
            //drawnActor3D.EffectParameters.Texture = textureDictionary["right"];
            //drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 20);
            //drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, -90, 0);
            //drawnActor3D.Transform3D.Translation = new Vector3(worldScale / 2.0f, 0, 0);
            //objectManager.Add(drawnActor3D);

            ////top
            //drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            //drawnActor3D.ActorType = ActorType.Sky;
            //drawnActor3D.ID = "sky top";
            //drawnActor3D.EffectParameters.Texture = textureDictionary["sky"];
            //drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            //drawnActor3D.Transform3D.RotationInDegrees = new Vector3(90, -90, 0);
            //drawnActor3D.Transform3D.Translation = new Vector3(0, worldScale / 2.0f, 0);
            //objectManager.Add(drawnActor3D);

            ////front
            //drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            //drawnActor3D.ActorType = ActorType.Sky;
            //drawnActor3D.ID = "sky front";
            //drawnActor3D.EffectParameters.Texture = textureDictionary["front"];
            //drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            //drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 180, 0);
            //drawnActor3D.Transform3D.Translation = new Vector3(0, 0, worldScale / 2.0f);
            //objectManager.Add(drawnActor3D);
        }

        #endregion Initialization - Vertices, Archetypes, Helpers, Drawn Content(e.g. Skybox)

        #region Load & Unload Game Assets

        protected override void LoadContent()
        {
        }

        protected override void UnloadContent()
        {
            //housekeeping - unload content
            textureDictionary.Dispose();
            modelDictionary.Dispose();
            fontDictionary.Dispose();
            modelDictionary.Dispose();
            soundManager.Dispose();

            base.UnloadContent();
        }

        #endregion Load & Unload Game Assets

        #region Update & Draw

        protected override void Update(GameTime gameTime)
        {
            bool turnOffResultTexts = false;

            if (myGameStateManager.Level1End)
            {
                //menuManager.SetScene("results");

                //timeResultText.StatusType = StatusType.Update | StatusType.Drawn;
                //timeTextObject.Transform2D.Translation = new Vector2(950, 500);
                //timeTextObject.Text = finalTime.ToString("0.00");
            }

            if (myGameStateManager.Level1Reset)
            {
                myGameStateManager.Level1Reset = false;

                finalTime = timerCount;
                Initialize();
                timerCount = 0;

                menuManager.SetScene("results");

                timeResultText.StatusType = StatusType.Drawn | StatusType.Update;
                resultTimeText.StatusType = StatusType.Drawn | StatusType.Update;
                resultTimeText.Text = finalTime.ToString("0.00");

                turnOffResultTexts = true;
            }

            if (myGameStateManager.PlayButtonPressed == false && turnOffResultTexts)
            {
                timeResultText.StatusType = StatusType.Off;
                resultTimeText.StatusType = StatusType.Off;
            }

            //IF THE CURRENT CAMERA IS THE INTRO AND IT IS AT THE FINISH LOCATION THEN CYCLE TO THE GAME CAMERA
            if (cameraManager.ActiveCamera.ID == GameConstants.Camera_NonCollidableCurveMainArena && cameraManager.ActiveCamera.Transform3D.Translation == finalCurveALocation)
            {
                levelOneStarted = true;
                timeResultText.StatusType = StatusType.Off;
                resultTimeText.StatusType = StatusType.Off;
                //menuManager.Remove("main", (actor) => actor.ID.Equals("main"));
                EventDispatcher.Publish(new EventData(EventCategoryType.Level, EventActionType.OnStartTimer, null));
                propTetrahedron.Transform3D.Translation = new Vector3(300, 0, 0);
                propTetrahedron.StatusType = StatusType.Off;

                cameraManager.CycleActiveCamera();
                cameraManager.ActiveCamera.StatusType = StatusType.Update;
                
                tetrahedron.StatusType = StatusType.Update | StatusType.Drawn;

                EventDispatcher.Publish(new EventData(EventCategoryType.Player, EventActionType.OnTetraStart, null));

            }

            #region LEVEL TIMER
            if (levelOneStarted && myGameStateManager.StartTimer)
            {
                timerCount += (float)gameTime.ElapsedGameTime.TotalSeconds;
                timeTextObject.Text = timerCount.ToString("0.00");
            }

            if (myGameStateManager.TimerReset)
            {
                
                timerCount = 0;

                myGameStateManager.TimerReset = false;
            }
            #endregion LEVEL TIMER

            if (tetrahedron.Transform3D.Translation.X > 232f && tetrahedron.Transform3D.Translation.X < 242f
                && tetrahedron.Transform3D.Translation.Y < 30f
                && tetrahedron.Transform3D.Translation.Z < 145f && tetrahedron.Transform3D.Translation.Z > 135f)
            {
                tetrahedron.Transform3D.Translation = new Vector3(300, 0, 0);
                tetrahedron.StatusType = StatusType.Off;
                objectManager.Remove(tetrahedron);

                propHexahedron.Transform3D.Translation = new Vector3(300, 0, 0);
                propHexahedron.StatusType = StatusType.Off;

                hexahedron.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);
                hexahedron.Transform3D.Translation = new Vector3(237, 21, 140);
                hexahedron.StatusType = StatusType.Update | StatusType.Drawn;

                EventDispatcher.Publish(new EventData(EventCategoryType.Player, EventActionType.OnHexaStart, null));

                //attach a controller
                cameraManager.ActiveCamera.ControllerList.Add(new ThirdPersonController(
                    GameConstants.Controllers_CollidableThirdPerson,
                    ControllerType.ThirdPerson,
                    hexahedron,
                    120,
                    270,
                    1f,
                    mouseManager));

            }

            if (hexahedron.Transform3D.Translation.X > -35f && hexahedron.Transform3D.Translation.X < -25f
                && hexahedron.Transform3D.Translation.Y < 30f
                && hexahedron.Transform3D.Translation.Z < 145f && hexahedron.Transform3D.Translation.Z > 135f)
            {
                hexahedron.Transform3D.Translation = new Vector3(320, 0, 0);
                hexahedron.StatusType = StatusType.Off;
                objectManager.Remove(hexahedron);

                propOctahedron.Transform3D.Translation = new Vector3(320, 0, 0);
                propOctahedron.StatusType = StatusType.Off;


                octahedron.Transform3D.RotationInDegrees = new Vector3(0, 45, 0);
                octahedron.Transform3D.Translation = new Vector3(-30, 23, 140);
                octahedron.StatusType = StatusType.Update | StatusType.Drawn;

                EventDispatcher.Publish(new EventData(EventCategoryType.Player, EventActionType.OnOctaStart, null));

                //attach a controller
                cameraManager.ActiveCamera.ControllerList.Add(new ThirdPersonController(
                    GameConstants.Controllers_CollidableThirdPerson,
                    ControllerType.ThirdPerson,
                    octahedron,
                    135,
                    270,
                    1f,
                    mouseManager));

            }

            #region Demo
#if DEMO

            #region Camera
            if (keyboardManager.IsFirstKeyPress(Keys.C))
            {
                cameraManager.CycleActiveCamera();
                EventDispatcher.Publish(new EventData(EventCategoryType.Camera,
                    EventActionType.OnCameraCycle, null));
            }
            #endregion Camera

#endif
            #endregion Demo

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(new Color(191, 206, 114)); // C64 Yellow
            GraphicsDevice.Clear(new Color(103, 182, 189));

            base.Draw(gameTime);
        }

        #endregion Update & Draw
    }
}